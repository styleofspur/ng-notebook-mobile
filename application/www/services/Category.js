ngNotebookServices.service('Category', ['$q', '$http', 'CategoryStorage', function ($q, $http, CategoryStorage) {

  return {

    /**
     *
     * @returns {promise|*|promise|promise|promise|Function}
     */
    getTopParents: function () {

      var res = $q.defer();

      $http.get('http://localhost:3333/api/categories/top-parents')
          .success(function (result) {

            if (result.status === 'success') {
              CategoryStorage.updateTopParentsList(result.data.categories);
              res.resolve(result.data.categories);
            } else res.resolve([]);

          }.bind(this))
          .error(function (result) {
            console.error('get categories top parents', result);
            res.resolve(result ? result.message : CategoryStorage.getTopParentsList());
          });

      return res.promise;

    },

    /**
     *
     * @param id
     * @returns {*}
     */
    getChildren: function (id) {

      var res = $q.defer();

      $http.get('http://localhost:3333/api/categories/' + id + '/children')
            .success(function (result) {

              if (result.status === 'success') {

                CategoryStorage.updateChildrenCategoriesList({
                  parentId: id,
                  children: result.data.children
                });

                res.resolve(result.data.children);

              } else res.resolve([]);

            }.bind(this))
            .error(function (result) {
              console.error('get category children', result);

              res.resolve(result ? result.message : CategoryStorage.getCategoriesChildrenList(id));

            });

      return res.promise;

    },

    /**
     *
     * @param id
     * @returns {*}
     */
    getSiblings: function (id) {

      var res = $q.defer();

      $http.get('http://localhost:3333/api/categories/' + id + '/siblings')
            .success(function (result) {

              if (result.status === 'success') {

                CategoryStorage.updateSiblingsCategoriesList({
                  siblingId: id,
                  siblings:  result.data.siblings
                });

                res.resolve(result.data.siblings);
              } else res.resolve([]);

            }.bind(this))
            .error(function (result) {
               console.error('get category siblings', result);

               res.resolve(result ? result.message : CategoryStorage.getCategoriesSiblingsList(id));

            });

     return res.promise;

    }
  };
}]);