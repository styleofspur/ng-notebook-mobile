'use strict';

var ngNotebookServices = angular.module('ngNotebook.services', [
  'ngNotebook.servicesLocalStorage',
  'ngNotebook.utils'
]);