ngNotebookServices.service('Tag', ['$q', '$http', 'TagStorage', function ($q, $http, TagStorage) {

  return {

    /**
     *
     * @returns {promise|*|promise|promise|promise|Function}
     */
    getAll: function () {

      var res = $q.defer();

      $http.get('http://localhost:3333/api/tags')
        .success(function (result) {
          if (result.status === 'success') {
            TagStorage.updateList(result.data.tags);
            res.resolve(result.data.tags);
        } else res.resolve([]);
      }.bind(this))
        .error(function (result) {
          console.error('get all tags', result);
          res.resolve(result ? result.message : TagStorage.getList());
        });

      return res.promise;

    },

    /**
     *
     * @returns {promise|*|promise|promise|promise|Function}
     */
    createTag: function (tag) {

      var res = $q.defer();

      $http.post('http://localhost:3333/api/tags', {tag: tag})
          .success(function (result) {
            if (result.status === 'success') {
              TagStorage.updateList(result.data.tags);
              res.resolve(result.data.tags);
            } else res.resolve([]);
          }.bind(this))
          .error(function (result) {
            console.error('create tag', result);
            res.resolve(result ? result.message : TagStorage.add(tag));
          });

      return res.promise;

    },

    /**
     *
     * @returns {promise|*|promise|promise|promise|Function}
     */
    editTag: function (oldTag, newTag) {

      var res = $q.defer();

      $http.put('http://localhost:3333/api/tag/'+oldTag, {tag: newTag})
          .success(function (result) {
            if (result.status === 'success') {
              TagStorage.edit(oldTag, newTag.name);
              res.resolve();
            } else res.reject();
          })
          .error(function (result) {
            console.error('delete tag', result);
            result ? res.reject(result.message) : res.resolve(TagStorage.edit(oldTag, newTag.name));
          });

      return res.promise;

    },

    /**
     *
     * @returns {promise|*|promise|promise|promise|Function}
     */
    deleteTag: function (tag) {

      var res = $q.defer();

      $http.delete('http://localhost:3333/api/tags/' + tag)
          .success(function (result) {
            if (result.status === 'success') {
              TagStorage.updateList(result.data.tags);
              res.resolve(result.data.tags);
            } else res.resolve([]);
          }.bind(this))
          .error(function (result) {
            console.error('delete tag', result);
            res.resolve(result ? result.message : TagStorage.delete(tag));
          });

      return res.promise;

    }
  };
}]);