'use strict';

var ngNotebookServicesLocalStorage = angular.module('ngNotebook.servicesLocalStorage', [
  'ngNotebook.utils',
  'ngNotebook.filters'
]);