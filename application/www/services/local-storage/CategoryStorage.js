ngNotebookServicesLocalStorage.service('CategoryStorage', ['$localStorage', function ($localStorage) {

  return {

    /**
     *
     * @param data
     */
    updateTopParentsList: function (data) {
      $localStorage.setCollection('categoriesTopParents', data);
    },

    /**
     *
     * @param data
     */
    getTopParentsList: function () {
      return $localStorage.getCollection('categoriesTopParents');
    },

    /**
     *
     * @param data
     */
    updateChildrenCategoriesList: function (data) {
      var localCategories = $localStorage.getCollection('categoriesChildren');
      if (localCategories.indexOf(data) === -1) {
        localCategories.push(data);
        $localStorage.setCollection('categoriesChildren', localCategories);
      }
    },

    /**
     *
     * @param parentId
     */
    getCategoriesChildrenList: function (parentId) {
      var categoriesChildren = $localStorage.getCollection('categoriesChildren'),
      children = [];

      categoriesChildren.some(function (child) {
        if (child.parentId === parentId) {
          children = child.children;
          return false;
        }
      });

      return children;

    },

    /**
     *
     * @param data
     */
    updateSiblingsCategoriesList: function (data) {
      var localCategories = $localStorage.getCollection('categoriesSiblings');
      if (localCategories.indexOf(data) === -1) {
        localCategories.push(data);
        $localStorage.setCollection('categoriesSiblings', localCategories);
      }
    },

    /**
     *
     * @param siblingId
     */
    getCategoriesSiblingsList: function (siblingId) {
      var categoriesSiblings = $localStorage.getCollection('categoriesSiblings'),
      siblings = [];

      categoriesSiblings.some(function (sibling) {
        if (sibling.siblingId === siblingId) {
          siblings = sibling.siblings;
          return false;
        }
      });

      return siblings;

    },

  }

}]);