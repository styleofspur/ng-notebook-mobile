'use strict';

ngNotebookServicesLocalStorage.service('ArticleStorage', ['$localStorage', '$sce', '$filter',
  function ($localStorage, $sce, $filter) {

    return {

      /**
       *
       * @param by
       * @param data
       * @returns {*}
       */
      getBy: function (by, data) {
        return this['$getBy' + $filter('capitalize')(by) ](data);
      },

      /**
       *
       * @returns {*}
       */
      getList: function () {
        return $localStorage.getCollection('articles')
      },

      /**
       *
       * @param data
       */
      updateList: function (data) {
        $localStorage.setCollection('articles', data);
      },

      /**
       *
       * @param data
       */
      add: function (data) {
        var articles = $localStorage.getCollection('articles'),
        newArticle = angular.extend({
          "_id": Date.now().toString()
        }, data);

        $localStorage.update('createdArticles', data);
        articles.push(newArticle);
        $localStorage.setCollection('articles', articles);

        return newArticle;
      },

      /**
       *
       * @param id
       */
      delete: function (id) {

        $localStorage.addToCollection('deletedArticles', id);
        $localStorage.deleteFromCollection('articles', {
          "_id": id
        });

        return id;
      },

      /**
       *
       * @param id
       * @param data
       */
      edit: function (id, data) {
        var articles = $localStorage.getCollection('articles'),
        article = null;

        articles.some(function (artcl, i) {
          if (artcl._id === id) {
            article = articles[i];
            return false;
          }
        });

        article = angular.extend(article, data);
        $localStorage.setCollection('articles', articles);

        return article;
      },

      /**
       *
       */
      updateTags: function(oldTag, newTag) {

        var articles = $localStorage.getCollection('articles');

        articles.forEach(function (article, i) {
          var j = article.tags.indexOf(oldTag);
          if (article.tags && j !== -1) {
            articles[i].tags[j] = newTag;
          }
        });

        $localStorage.setCollection('articles', articles);
      },

      /**
       *
       * @param id
       */
      $getById: function (id) {
        var articles = $localStorage.getCollection('articles'),
        found = null;

        articles.some(function (article) {
          if (article._id === id) {
            found = article;
            return false;
          }
        });

        return found;

      },

      /**
       *
       * @param tags
       */
      $getByTags: function (tags) {
        var articles = $localStorage.getCollection('articles'),
        filtered = [],
        tags = tags.split(',');

        articles.forEach(function (article) {

          if (article.tags && article.tags.length) {

            tags.some(function (tag) {

              if (article.tags.indexOf(tag) !== -1 && filtered.indexOf(article) === -1) {
                filtered.push(article);
                return false;
              }

            });

          }

        });

        return filtered;

      },

      /**
       *
       * @param categories
       */
      $getByCategory: function (categories) {
        var articles = $localStorage.getCollection('articles'),
        categoriesTopParents = $localStorage.getCollection('categoriesTopParents'),
        categoriesChildren = $localStorage.getCollection('categoriesChildren'),
        categories = categories.split('/'),
        filtered = [],
        category = null;

        if (categories.length === 1) {

          categoriesTopParents.some(function (topParentCtg) {
            if (topParentCtg.name === categories[0]) {
              category = topParentCtg;
              return false;
            }
          });

        } else {

          var categoryName = categories.splice(categories.length - 1, 1)[0];
          categoriesChildren.some(function (childCtg) {
            childCtg.children.forEach(function (child) {
              if (child.path === categories.join('/') && child.name == categoryName) {
                category = child;
                return false;
              }
            });
            if (category) return false;
          });

        }

        articles.forEach(function (article) {
          if (article.category && ( (article.category._id || article.category.id) === category.id)) {
            filtered.push(article);
          }
        });

        return filtered;

      },

      /**
       *
       * @param txt
       */
      $getByTxt: function (txt) {
        var articles = $localStorage.getCollection('articles'),
        lcTxt        = txt.toLowerCase(),
        byTitle      = [],
        byContent    = [],
        byTags       = [],
        byCategory   = [];

        articles.forEach(function (article) {
          var lcTitle = article.title.toLowerCase(),
          escContent  = $filter('htmlToPlaintext')(article.content),
          lcCategory  = article.category && article.category.name ? article.category.name.toLowerCase() : '',
          lcContent   = escContent.toLowerCase(),
          titleIx     = lcTitle.indexOf(lcTxt),
          contentIx   = lcContent.indexOf(lcTxt),
          categoryIx  = lcCategory ? lcCategory.indexOf(lcTxt) : -1,
          tagsIx      = -1;

          // search by TITLE
          if (titleIx !== -1) {

            var titleSubstr = article.title.substring(titleIx, titleIx + txt.length);
            byTitle.push({
              _id:       article._id,
              type:      "title",
              substring: titleSubstr,
              value:     article.title
            });

          }

          // search by CONTENT
          if (contentIx !== -1) {
            var contentSubstr = escContent.substring(contentIx, contentIx + txt.length);
            byContent.push({
              _id:       article._id,
              type:      "content",
              substring: contentSubstr,
              value:     escContent
            });
          }

          if (article.tags && article.tags) {

            // search by TAG
            var similarTag = '',
            tagIx      = -1;
            article.tags.some(function (tag) {
              tagIx = tag.indexOf(txt.toLowerCase());
              if (tagIx !== -1) {
                similarTag = tag;
                return false;
              }
            });

            if (similarTag) {

              if (tagIx < 0) tagIx++;
              var tagSubstr = similarTag.substring(tagIx, tagIx + txt.length);

              byTags.push({
                _id:     article._id,
                type:    "tag",
                substring: tagSubstr,
                value:     similarTag,
                preview:   article.title.substring(0, 32)
              });

            }

          }

          // search by CATEGORY
          if (categoryIx !== -1) {
            var categorySubstring = article.category.name.substring(categoryIx, categoryIx + txt.length);

            byCategory.push({
              _id:       article._id,
              type:      "category",
              substring: categorySubstring,
              value:     article.category.name,
              preview:   article.title.substring(0, 32)
            });
          }

        });

        return byTitle.concat(byContent).concat(byCategory).concat(byTags);
      },

      /**
       *
       * @param value
       */
      $getByDate: function (value) {
        var parts    = value.split('/'),
            by       = parts[0],
            cond     = parts[1],
            val      = parts[2],
            result   = [],
            articles = $localStorage.getCollection('articles');

        articles.forEach(function (article) {

          switch (cond) {

            case 'before':
            {
              if ( moment(article[by+'At']).isBefore(val) ) {
                article.content = $filter('htmlToPlaintext')(article.content).substring(0, 128);
                result.push(article);
              }
              break;
            }

            case 'before-or-at':
            {
              if (
                  moment(article[by+'At']).isBefore(val) ||
                  moment(article[by+'At']).format() === moment(val).format()
              ) {
                article.content = $filter('htmlToPlaintext')(article.content).substring(0, 128);
                result.push(article);
              }
              break;
            }

            case 'after':
            {
              if ( moment(article[by+'At']).isAfter(val) ) {
                article.content = $filter('htmlToPlaintext')(article.content).substring(0, 128);
                result.push(article);
              }
              break;
            }

          }

        });

        return result;

      }

    };

  }]);