ngNotebookServicesLocalStorage.service('TagStorage', ['$localStorage', 'ArticleStorage',
  function ($localStorage, ArticleStorage) {

    return {

      /**
       *
       * @returns {*}
       */
      getList: function () {
        return $localStorage.getCollection('tags');
      },

      /**
       *
       * @param data
       */
      updateList: function (data) {
        $localStorage.setCollection('tags', data);
      },

      /**
       *
       * @param tag
       */
      add: function (tag) {
        var tags = $localStorage.getCollection('tags');

        $localStorage.update('createdTags', tag);
        tags.push(tag);
        $localStorage.setCollection('tags', tags);

        return tag;
      },

      /**
       *
       * @param tag
       */
      delete: function (tag) {

        var tags = $localStorage.getCollection('tags');

        $localStorage.addToCollection('deletedTags', tag);
        tags.splice(tags.indexOf(tag), 1);
        $localStorage.setCollection('tags', tags);

        return tags;
      },

      /**
       *
       * @param oldTag
       * @param newTag
       */
      edit: function (oldTag, newTag) {
        var tags = $localStorage.getCollection('tags'),
        oldI = tags.indexOf(oldTag);

        tags[oldI] = newTag;
        $localStorage.setCollection('tags', tags);

        ArticleStorage.updateTags(oldTag, newTag);

        return newTag;
      }

    }

  }]);