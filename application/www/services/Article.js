ngNotebookServices.service('Article', ['$q', '$http', '$localStorage', 'ArticleStorage',
  function ($q, $http, $localStorage, ArticleStorage) {

  return {

    itemsPerPage: 5,

    /**
     *
     * @returns {*|Array|string|Blob|Query}
     */
    getAll: function () {
      var res = $q.defer();

      $http.get('http://localhost:3333/api/articles')
          .success(function (result) {
            if (result.status === 'success') {
              ArticleStorage.updateList(result.data.articles);
              res.resolve(result.data.articles);
            } else res.resolve([]);
          })
          .error(function (result) {
            console.error('get all articles', result);
            res.resolve(result ? result.message : ArticleStorage.getList());
          });

      return res.promise;
    },

    /**
     *
     * @param id
     * @returns {*}
     */
    getById: function (id) {
      var res = $q.defer();

      $http.get('http://localhost:3333/api/article/' + id)
          .success(function (result) {
            if (result.status === 'success') {
              res.resolve(result.data.article);
            } else res.resolve([]);
          })
          .error(function (result) {
            console.error('get article by id', result);
            res.resolve(result ? result.message : (ArticleStorage.getBy("id", id) || null));
          });

      return res.promise;
    },

    /**
     *
     * @param id
     * @param data
     * @returns {promise|*|promise|promise|promise|Function}
     */
    editArticle: function (id, data) {

      var res = $q.defer();

      $http.put('http://localhost:3333/api/article/' + id, {article: data})
          .success(function (result) {
            if (result.status === 'success') {
              res.resolve(result.data.article);
            } else res.resolve([]);
          })
          .error(function (result) {
            console.error('edit article', result);
            ArticleStorage.edit(id, data);
            res.resolve(result ? result.message : angular.extend({
              _id: id
            }, data));
          });

      return res.promise;
    },

    /**
     *
     * @param data
     * @returns {promise|*|promise|promise|promise|Function}
     */
    createArticle: function (data) {

      var res = $q.defer();

      $http.post('http://localhost:3333/api/article/create', {article: data})
          .success(function (result) {
            if (result.status === 'success') {
              res.resolve(result.data.article);
            } else res.resolve([]);
          })
          .error(function (result) {
            console.error('create article', result);
            var newArticle = ArticleStorage.add(data);
            res.resolve(result ? result.message : newArticle);
          });

      return res.promise;
    },

    /**
     *
     * @param id
     * @returns {promise|*|promise|promise|promise|Function}
     */
    deleteArticle: function (id) {

      var res = $q.defer();

      $http.delete('http://localhost:3333/api/article/' + id)
          .success(function (result) {
            if (result.status === 'success') {
              res.resolve(result.data.article);
            } else res.resolve([]);
          })
          .error(function (result) {
            console.error('delete article', result);
            res.resolve(result ? result.message : ArticleStorage.delete(id));
          });

      return res.promise;
    },

    /**
     *
     * @param by
     * @param value
     */
    searchBy: function(by, value) {

      var res = $q.defer();

      $http.get('http://localhost:3333/api/articles/' + by + '/' + value)
          .success(function (result) {
            if (result.status === 'success') {
              res.resolve(result.data.articles);
            } else res.resolve([]);
          })
          .error(function (result) {
            console.error('search articles by ' + by + ': ' + value, result);
            res.resolve(result ? result.message : ArticleStorage.getBy(by, value));
          });

      return res.promise;

    }
  };

}]);