'use strict';

/**
 *
 */
ngNotebookFilters.filter('htmlToPlaintext', function() {
      return function(text) {
        return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
      };
    }
);