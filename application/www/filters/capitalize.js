'use strict';

/**
 *
 */
ngNotebookFilters.filter('capitalize', function() {
      return function(text) {
        return  text ? text.charAt(0).toUpperCase() + text.slice(1) : '';
      };
    }
);