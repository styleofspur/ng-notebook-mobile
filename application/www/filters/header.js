'use strict';

/**
 *
 */
ngNotebookFilters.filter('header', function() {
      return function(text) {

        if (text.indexOf('/') !== -1) {
          var parts  = text.split('/');

          if (parts[parts.length - 1].indexOf('|') !== 1) {
            parts.concat(parts[parts.length - 1].split('|'));
          }

          text = parts.join(' ');

        }

        return text;
      };
    }
);