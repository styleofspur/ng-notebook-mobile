'use strict';

ngNotebookCalendar.controller('NgNotebookCalendarCtrl', ['$scope', '$state', '$ionicHistory', '$filter',
  function ($scope, $state, $ionicHistory, $filter) {

    $scope.moment = moment;

    $scope.$emit('setNewPageHeader', {
      header: 'Calendar'
    });

    $scope.filterData = {
      type: '',
      cond: '',
      value: ''
    };

    $scope.filterTypes = [
      {
        type: 'created',
        label: 'Creation'
      },
      {
        type: 'updated',
        label: 'Update'
      }
    ];

    $scope.filterConds = [
      {
        cond: 'before',
        label: 'Before'
      },
      {
        cond: 'before-or-at',
        label: 'Before or at'
      },
      {
        cond: 'before-and-at',
        label: 'Before and at'
      },
      {
        cond: 'at',
        label: 'At'
      },
      {
        cond: 'at-or-after',
        label: 'At or after'
      },
      {
        cond: 'at-and-after',
        label: 'At and after'
      },
      {
        cond: 'after',
        label: 'After'
      },
      {
        cond: 'between',
        label: 'Between'
      }
    ];

    $scope.dateModel = {
      simple: '',
      start: '',
      end: ''
    };

    /**
     *
     */
    $scope.setType = function () {
      $scope.filterData.cond = '';
      $scope.filterData.value = '';
      $scope.dateModel = {
        simple: '',
        start: '',
        end: ''
      };
    };

    /**
     *
     */
    $scope.setCond = function () {
      $scope.filterData.value = '';
      $scope.dateModel = {
        simple: '',
        start: '',
        end: ''
      };
    };

    /**
     *
     * @param e
     */
    $scope.setDateVal = function (order) {

      if (order === 'start') {

        if ($scope.filterData.value.indexOf('-') >= 0) {
          var parts = $scope.filterData.value.split('-');
          $scope.filterData.value = moment($scope.dateModel.start).format('MM.DD.YYYY') + '-' + parts[1];
        } else {
          $scope.filterData.value = moment($scope.dateModel.start).format('MM.DD.YYYY') + '-';
        }

      }
      else if (order === 'end') {

        if ($scope.filterData.value.indexOf('-') >= 0) {
          var parts = $scope.filterData.value.split('-');
          $scope.filterData.value = parts[0] + '-' + moment($scope.dateModel.end).format("MM.DD.YYYY");
        } else {
          $scope.filterData.value = '-' + moment($scope.dateModel.end).format("MM.DD.YYYY");
        }

      } else {
        $scope.filterData.value = moment($scope.dateModel.simple).format('MM.DD.YYYY');
      }

    };

    /**
     * Closes the modal window, resets export data model and clears watchers.
     */
    $scope.filter = function () {

      $state.go('search-results', {

        by: 'date',
        value: [
          $scope.filterData.type, $scope.filterData.cond, $scope.filterData.value
        ].join('/')

      });
      $ionicHistory.nextViewOptions({
        historyRoot: false
      });

    };

  }]);