var ngNotebook = angular.module('ngNotebook', [
  'ionic',
  'ngNotebook.core',
  'ngNotebook.articles',
  'ngNotebook.article',
  'ngNotebook.CKEditor',
  'ngNotebook.tags',
  'ngNotebook.calendar',
  'ngNotebook.search',
  'ngNotebook.categoriesTree',
  'ngNotebook.searchResults'
])
    .run(function($ionicPlatform, $rootScope) {
      $ionicPlatform.ready(function() {
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
          StatusBar.styleLightContent();
        }
      });

      var connWtch = $rootScope.$watch(function () {
        return navigator.connection;
      }, function (val) {
        if (val) {
          $rootScope.hasConnection = val.type !== "none";
          connWtch();
          console.log('Connection', $rootScope);
        }
      });

    })
    .config(function($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider) {

      function valToString(val)   { return val != null ? val.toString() : val; }
      function valFromString(val) { return val != null ? val.toString() : val; }
      function regexpMatches(val) { return this.pattern.test(val);             }
      $urlMatcherFactoryProvider.type("nonEncodedURI", {
        encode:  valToString,
        decode:  valFromString,
        is:      regexpMatches,
        pattern: /[a-zA-Z0-9/\%]/
      });


      $stateProvider
          .state('root', {
            url: "/",
            controller: "NgNotebookArticlesCtrl",
            controllerAs: "ngnArticlesCtrl",
            templateUrl: "articles/ngNotebookArticles.html"
          })
          .state('articles', {
            url: "/articles",
            controller: "NgNotebookArticlesCtrl",
            controllerAs: "ngnArticlesCtrl",
            templateUrl: "articles/ngNotebookArticles.html"
          })
          .state('article', {
            url: "^/article/:id",
            controller: "NgNotebookArticleCtrl",
            controllerAs: "ngnArticleCtrl",
            templateUrl: "article/ngNotebookArticle.html"
          })
          .state('create', {
            url: "^/create",
            controller: "NgNotebookCreateArticleCtrl",
            controllerAs: "ngnCreateArticleCtrl",
            templateUrl: "article/create-article/ngNotebookCreateArticle.html"
          })
          .state('tags', {
            url: "^/tags",
            controller: "NgNotebookTagsCtrl",
            controllerAs: "ngnTagsCtrl",
            templateUrl: "tags/ngNotebookTags.html"
          })
          .state('calendar', {
            url: "^/calendar",
            controller: "NgNotebookCalendarCtrl",
            controllerAs: "ngnCalendarCtrl",
            templateUrl: "calendar/ngNotebookCalendar.html"
          })
          .state('search', {
            url: "^/search/:searchTxt?",
            controller: "NgNotebookSearchCtrl",
            controllerAs: "ngnSearchCtrl",
            templateUrl: "search/ngNotebookSearch.html"
          })
          .state('search-results', {
            url: "^/search/{by:nonEncodedURI}/{value:nonEncodedURI}",
            controller: "NgNotebookSearchResultsCtrl",
            controllerAs: "ngnSearchResultsCtrl",
            templateUrl: "search-results/ngNotebookSearchResults.html"
          });

      $urlRouterProvider.otherwise('/');
    });