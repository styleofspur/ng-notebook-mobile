/**
 * @ngdoc controller
 * @name ArticlesCtrl
 *
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $scope
 * */
ngNotebookArticles.controller('NgNotebookArticlesCtrl', ['$scope', 'Article', '$sce', '$state', '$ionicPopup',
    function($scope, Article, $sce, $state, $ionicPopup) {

    $scope.isReady = false;

    Article.getAll().then(function(data) {

        data.forEach(function(item) {
            item.content = $sce.trustAsHtml(
              item.content.length > 256 ? item.content.substr(0, 256) + '...' : item.content
            );
        });

        $scope.articles = data;
        $scope.isReady  = true;

    });

    /**
     *
     */
    $scope.doRefresh = function() {
        $state.reload();
        $scope.$broadcast('scroll.refreshComplete');
    };

    // On delete btn click
    $scope.deleteArticle = function(id) {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Delete Article',
            template: 'Are you sure you want to delete this article?'
        });
        confirmPopup.then(function(res) {

            if (res) {

                Article.deleteArticle(id).then(function() {
                    $scope.articles.some(function(article, i) {
                        if (article._id === id) {
                            $scope.articles.splice(i, 1);
                            return false;
                        }
                    })
                });

            }

        });
    };

}]);
