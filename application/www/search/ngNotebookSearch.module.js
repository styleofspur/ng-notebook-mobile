'use strict';

var ngNotebookSearch = angular.module('ngNotebook.search', [
    'ngNotebook.services',
    'ngNotebook.filters'
]);