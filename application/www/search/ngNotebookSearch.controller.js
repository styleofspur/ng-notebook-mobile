/**
 * @ngdoc controller
 * @name SearchCtrl
 *
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $scope
 * */
ngNotebookSearch.controller('NgNotebookSearchCtrl', ['$scope', 'Article', '$state', 'ngNotebookSearchResultsFormatter',
  function ($scope, Article, $state, SearchResultsFormatter) {

    $scope.$emit('setNewPageHeader', {
      header: 'Search'
    });

    $scope.results   = [];
    $scope.searchTxt = $state.params.searchTxt || '';
    if ($scope.searchTxt) getResults();

    /**
     *
     */
    $scope.onChange = function () {

      if ($scope.searchTxt) {
        getResults();
      }

    };

    /**
     *
     * @param event
     */
    $scope.onSubmit = function(event) {


    };

    function getResults() {
      Article.searchBy('txt', $scope.searchTxt).then(function (results) {
        $scope.results = SearchResultsFormatter.formatResults(results);
      });
    }

  }]);
