'use strict';

ngNotebookSearch.service('ngNotebookSearchResultsFormatter', function ($sce) {

  /**
   *
   * @param escContent
   * @param contentSubstr
   * @returns {string}
   */
  function prepareFoundArticleContent(escContent, contentSubstr) {
    var dots = '...',
    matched  = '<span class="search__result__string search__result__content">' + contentSubstr + '</span>',
    start    = escContent.substr(escContent.indexOf(contentSubstr) - 10, 10),
    end      = escContent.substr(escContent.lastIndexOf(contentSubstr) + 10, 10);

    return $sce.trustAsHtml(dots + start + matched + end + dots);

  }

  return {

    /**
     *
     * @param results
     * @returns {Array}
     */
    formatResults: function(results) {
      var formatted = [];

      if (results && results.length) {

        results.forEach(function (result) {

          formatted.push(this['$format' + result.type.charAt(0).toUpperCase() + result.type.slice(1) + 'Results'](result));

        }.bind(this));

      }

      return formatted;

    },

    /**
     *
     * @param result
     * @returns {*}
     */
    $formatTitleResults: function(result) {

      result.value = $sce.trustAsHtml(
          result.value.replace(
              result.substring,
              '<span class="search__result__string search__result__title">' + result.substring + '</span>'
          )
      );

      return result;
    },

    /**
     *
     * @param result
     * @returns {*}
     */
    $formatContentResults: function(result) {

      result.value = prepareFoundArticleContent(result.value, result.substring);

      return result;
    },

    /**
     *
     * @param result
     * @returns {*}
     */
    $formatTagResults: function(result) {

      result.value = $sce.trustAsHtml('<strong>'+result.value.replace(
          result.substring,
          '<span class="search__result__string search__result__tag">' + result.substring + '</span>'
      )+'</strong>: ' + result.preview);

      return result;
    },

    $formatCategoryResults: function(result) {
      result.value = $sce.trustAsHtml('<strong>'+result.value.replace(
              result.substring,
              '<span class="search__result__string search__result__category">' + result.substring + '</span>'
          )+'</strong>: ' + result.preview);

      return result;
    }

  };

});