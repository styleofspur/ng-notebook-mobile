ngNotebookCKEditor.directive('ckEditor', function($timeout) {
  return {
    require: '?ngModel',
    link: function(scope, elm, attr, ngModel) {

      scope.editorName = '' + Date.now();
      $timeout(function () {

        scope.$watch(function() {
          return ngModel;
        }, function (model) {

          if (model) {

            $timeout(function () {
              CKEDITOR.replace(scope.editorName);
              init();
            });

          }
        });
      }, 0);

      /**
       *
       */
      function init() {
        var ck = CKEDITOR.instances[scope.editorName];

        if (!ngModel) return;

        ck.on('instanceReady', function() {
          //console.log('CKEDITOR initial model', ngModel);
          ck.setData(ngModel.$viewValue);
        });

        function updateModel() {
          //console.log('CKEDITOR updated model', ck.getData());
          scope.$apply(function() {
            ngModel.$setViewValue(ck.getData());
          });
        }

        ck.on('change', updateModel);
        ck.on('key', updateModel);
        ck.on('dataReady', updateModel);

        ngModel.$render = function(value) {
          ck.setData(ngModel.$viewValue);
        };
      }


      scope.$on("$stateChangeStart", function() {
        CKEDITOR.instances[scope.editorName].destroy();
      });

    }
  };
});