/**
 * @ngdoc controller
 * @name CategoriesTree
 *
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $scope
 * */
ngNotebookCategoriesTree.controller('NgNotebookCategoriesTreeCtrl', ['$scope', 'Category', '$state', '$ionicHistory',
  function ($scope, Category, $state, $ionicHistory) {

    this.hasParent      = false;
    this.parentId       = '';
    this.categoriesPath = '';

    Category.getTopParents().then(function (data) {
      this.categories = data;
    }.bind(this));

    /**
     *
     */
    this.doSearch = function (name) {
      $state.go('search-results', {
        by: 'category',
        value: this.categoriesPath.length ? this.categoriesPath + '/' + name : name
      });
      $ionicHistory.nextViewOptions({
        historyRoot: false
      });
    };

    /**
     *
     * @param name
     */
    this.showChildren = function (category) {
      Category.getChildren(category.id).then(function (data) {
        this.categories     = data;
        this.hasParent      = true;
        this.parentId       = category.id;
        this.categoriesPath = category.name;
        console.log(this.categoriesPath);
      }.bind(this));
    };

    /**
     *
     * @param id
     */
    this.showParents = function (id) {
      Category.getSiblings(id).then(function (data) {
        this.categories     = data;
        this.hasParent      = !!data[0].parentId;
        this.parentId       = data[0].parentId || '';

        var parts = this.categoriesPath.split('/');
        this.categoriesPath = parts.splice(parts.length - 1, 1).join('/');
        console.log(this.categoriesPath);
      }.bind(this));
    }


  }]);
