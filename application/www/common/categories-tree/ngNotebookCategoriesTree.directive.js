'use strict';

ngNotebookCategoriesTree.directive('categoriesTree', function(){

  return {

    restrict: "E",

    templateUrl: 'common/categories-tree/ngNotebookCategoriesTree.html',

    controller: 'NgNotebookCategoriesTreeCtrl as ngnCtc'

  }

});