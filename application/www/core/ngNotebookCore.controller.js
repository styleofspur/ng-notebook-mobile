'use strict';

ngNotebookCore.controller('NgNotebookCoreCtrl', function ($scope, $ionicSideMenuDelegate, $state, $ionicHistory, $filter) {

  this.flags = {
    displayed: {
      navBtn:    true,
      addBtn:    true,
      searchBtn: true,
      homeBtn:   true
    }
  };

  var defaultHeader = "NG-NOTEBOOK";
  this.header = defaultHeader;
  this.type   = '';

  $scope.$on('setNewPageHeader', function (e, data) {
    this.header = $filter('header')(data.header);
    this.type   = data.type;
  }.bind(this));

  /**
   *
   */
  $scope.$on('$stateChangeSuccess', function(e, newState) {

    var isHomePage = ['root', 'articles'].indexOf(newState.name) !== -1;

    this.flags.displayed.navBtn    =
    this.flags.displayed.addBtn    =
    this.flags.displayed.searchBtn = isHomePage;

    this.flags.displayed.homeBtn   = !isHomePage;

    if (isHomePage) {
      this.header = defaultHeader;
      this.type   = '';
    }

  }.bind(this));

  /**
   *
   */
  this.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleRight();
  };

  /**
   *
   * @param path
   */
  this.goTo = function(path) {
    $state.go(path);
    $ionicHistory.nextViewOptions({
      historyRoot: false
    });
  }

});