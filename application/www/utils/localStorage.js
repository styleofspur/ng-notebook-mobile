'use strict';

ngNotebookUtils.factory('$localStorage', ['$window', function ($window) {
  return {
    set: function (key, value) {
      $window.localStorage[key] = value;
    },
    get: function (key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function (key, value) {
      this.set(key, JSON.stringify(value));
    },
    setCollection: function (key, value) {
      this.set(key, JSON.stringify(value));
    },
    getObject: function (key) {
      return JSON.parse($window.localStorage[key] || '{}');
    },
    getCollection: function (key) {
      return JSON.parse($window.localStorage[key] || '[]');
    },

    /**
     *
     * @param key
     * @param updateData
     */
    update: function (key, updateData) {

      var localData = this.getCollection(key);

      if (localData.length) {

        localData.indexOf(updateData) === -1 ?
            localData.push(updateData) :
            localData[localData.indexOf(updateData)] = updateData;

        this.setCollection(key, localData);

      } else {
        this.setCollection(key, [updateData]);
      }

    },

    /**
     *
     * @param key
     * @param cond
     * @param prop
     * @returns {*}
     */
    getFromCollection: function (key, cond, prop) {
      var localData = this.getCollection(key);

      if (localData.length) {
        var data;
        localData.some(function (localItem) {

          Object.keys(cond).some(function (key) {
            if (localItem[key] === cond[key]) {
              data = prop ? localItem[prop] : localItem;
              return false;
            }
          });

          if (data) return false;

        });

        return data || null;
      } else return null;

    },

    /**
     *
     * @param key
     * @param data
     */
    addToCollection: function (key, data) {
      var localData = this.getCollection(key);
      if (localData.indexOf(data) === - 1) localData.push(data);
      this.setCollection(key, localData);
    },

    /**
     *
     * @param key
     * @param data
     */
    deleteFromCollection: function (key, data) {

      var localData = this.getCollection(key);

      if (localData.length) {
        // when it's an existing item
        if (localData.indexOf(data) !== - 1) {
          localData.splice(localData.indexOf(data), 1);
        }
        // when its condition for finding
        else if (angular.isObject(data)) {

          var cond = data;
          localData.some(function (localItem, i) {

            var wasUpdate = false;
            Object.keys(cond).forEach(function (key) {
              if (localItem[key] === cond[key]) {
                localData.splice(i, 1);
                wasUpdate = true;
                return false;
              }
            });

            if (wasUpdate) return false;
          });

        }

        this.setCollection(key, localData);
      }

    },

    /**
     *
     * @param key
     * @param cond
     * @param data
     */
    editInCollection: function (key, cond, data) {
      var localData = this.getCollection(key);

      if (localData.length) {

        localData.some(function (localItem, i) {
          var wasUpdated;
          Object.keys(cond).some(function (key) {
            if (localItem[key] === cond[key]) {
              var c        = {};
              c[key]       = cond[key];
              localData[i] = angular.extend(c, data);
              wasUpdated   = true;
              return false;
            }
          });

          if (wasUpdated) return false;

        });

      } else {
        localData.push(data);
      }

      this.setCollection(key, localData);

    },

  }
}]);