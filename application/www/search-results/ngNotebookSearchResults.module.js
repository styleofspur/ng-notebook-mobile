'use strict';

var ngNotebookSearchResults = angular.module('ngNotebook.searchResults', [
    'ngNotebook.services',
    'ngNotebook.filters'
]);