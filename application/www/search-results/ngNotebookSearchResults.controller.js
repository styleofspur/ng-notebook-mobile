/**
 * @ngdoc controller
 * @name SearchResultsCtrl
 *
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $scope
 * */
ngNotebookSearchResults.controller('NgNotebookSearchResultsCtrl', ['$scope', 'Article', '$state', '$filter',
  function ($scope, Article, $state, $filter) {

    $scope.$emit('setNewPageHeader', {
      header: $filter('capitalize')($state.params.value),
      type:   $state.params.by
    });

    Article.searchBy($state.params.by, $state.params.value).then(function (articles) {
      $scope.articles = articles;
    });

  }]);
