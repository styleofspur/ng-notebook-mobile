/**
 * @ngdoc controller
 * @name TagsFormCtrl
 *
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $scope
 * */
ngNotebookTags.controller('NgNotebookTagsFormCtrl', ['$scope', 'Tag', '$state', '$ionicPopup', '$ionicHistory',
  function ($scope, Tag, $state, $ionicPopup, $ionicHistory) {

    $scope.tagUnderEdit = '';
    $scope.selectedTags = [];
    $scope.isReady      = false;

    Tag.getAll().then(function (data) {

      $scope.tags = data.sort(function (prev, next) {
        if (prev > next) return 1;
        if (next > prev) return -1;
        return 0;
      });
      $scope.searchableTag = '';
      $scope.displayedTags = angular.copy($scope.tags);
      $scope.isReady       = true;

    });

    /**
     *
     */
    $scope.doSearch = function () {
      $state.go('search-results', {
        by:    'tags',
        value: $scope.selectedTags.join(',')
      });
      $ionicHistory.nextViewOptions({
        historyRoot: false
      });
    };

    /**
     *
     */
    $scope.onChange = function () {

      if ($scope.AddTagForm.$valid) {

        if ($scope.searchableTag) {
          $scope.displayedTags = [];
          filterTags();
        } else {
          $scope.displayedTags = angular.copy($scope.tags).sort(function (prev, next) {
            if (prev > next) return 1;
            if (next > prev) return -1;
            return 0;
          });
        }

      }
      // show no tags if the searchable one has an error
      else if ($scope.AddTagForm.$invalid) {
        $scope.displayedTags = [];
      }

    };

    /**
     *
     * @param event
     */
    $scope.onSubmit = function(event) {

      if ($scope.AddTagForm.$valid && $scope.searchableTag && $scope.tags.indexOf($scope.searchableTag) === -1) {
        Tag.createTag($scope.searchableTag).then(function(result) {

          if (result) {
            $scope.selectedTags.push($scope.searchableTag);
            $scope.tags.push($scope.searchableTag);
            $scope.tags = $scope.tags.sort(function (prev, next) {
              if (prev > next) return 1;
              if (next > prev) return -1;
              return 0;
            });
            $scope.searchableTag = '';
            filterTags();
          }

        });
      }

    };

    /**
     *
     * @param tag
     */
    $scope.selectTag = function(tag) {
      if ($scope.selectedTags.indexOf(tag) === -1) {
        $scope.selectedTags.push(tag);
        $scope.displayedTags.splice($scope.displayedTags.indexOf(tag), 1);
      }
    };

    /**
     *
     * @param tag
     */
    $scope.removeSelectedTag = function (tag) {
      $scope.selectedTags.splice($scope.selectedTags.indexOf(tag), 1);
      filterTags();
    };

    /**
     *
     * @param tag
     */
    $scope.editTag = function(tag) {
      $scope.tagUnderEdit = tag;
    };

    /**
     *
     * @param tag
     */
    $scope.saveTag = function(tag) {

      if (tag !== $scope.tagUnderEdit) {

        Tag.editTag($scope.tagUnderEdit, {name: tag}).then(function(result) {

          $scope.tags.splice($scope.tags.indexOf($scope.tagUnderEdit), 1);
          $scope.tags.push(tag);
          $scope.tagUnderEdit = '';
          filterTags();

        });

      }

    };

    /**
     *
     * @param tag
     * @returns {boolean}
     */
    $scope.isUnderEdit = function(tag) {
      return $scope.tagUnderEdit === tag;
    };

    /**
     *
     */
    $scope.cancelEdit = function() {
      $scope.tagUnderEdit = '';
    };

    // On delete btn click
    $scope.deleteTag = function (tag) {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete Tag',
        template: 'Are you sure you want to delete this tag?'
      });
      confirmPopup.then(function (res) {

        if (res) {

          // local deletion
          Tag.deleteTag(tag).then(function(tags) {
            $scope.tags = tags.sort(function (prev, next) {
              if (prev > next) return 1;
              if (next > prev) return -1;
              return 0;
            });
            if ($scope.displayedTags.indexOf(tag) > -1) {
              $scope.displayedTags.splice($scope.displayedTags.indexOf(tag), 1);
            }
          });

        }

      });
    };

    /**
     *
     */
    function filterTags() {

      var beginWithSearchable = [],
      containSearchable   = [];

      $scope.tags.forEach(function (tag) {

        if ($scope.selectedTags.indexOf(tag) === -1) {

          if (tag.toLowerCase().indexOf($scope.searchableTag.toLowerCase()) === 0) {
            beginWithSearchable.push(tag);
          } else if (tag.toLowerCase().indexOf($scope.searchableTag.toLowerCase()) !== -1) {
            containSearchable.push(tag);
          }

        }

      });

      beginWithSearchable.sort(function (p, n) {
        return p > n;
      });

      containSearchable.sort(function (p, n) {
        return p > n;
      });

      $scope.displayedTags = beginWithSearchable.concat(containSearchable);
    }

  }]);
