/**
 * @ngdoc controller
 * @name TagsCtrl
 *
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $scope
 * */
ngNotebookTags.controller('NgNotebookTagsCtrl', ['$scope', 'Tag', '$state', '$ionicPopup', '$ionicHistory',
  function ($scope, Tag, $state, $ionicPopup, $ionicHistory) {

    $scope.$emit('setNewPageHeader', {
      header: 'Tags'
    });

  }]);
