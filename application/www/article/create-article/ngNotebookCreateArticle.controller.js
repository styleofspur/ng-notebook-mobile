'use strict';

ngNotebookCreateArticle.controller('NgNotebookCreateArticleCtrl', function($scope, Article, $state) {

  $scope.article = {
    title:   '',
    content: ''
  };

  /**
   *
   * @param e
   */
  $scope.onSubmit = function(e) {

    e.preventDefault();

    if ($scope.CreateArticleForm.$valid) {

      Article.createArticle($scope.article).then(function(result) {

        if (result) {
          $state.go('root');
        }

      });

    }

  }

});