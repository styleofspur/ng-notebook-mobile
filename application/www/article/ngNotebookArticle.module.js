'use strict';

var ngNotebookArticle = angular.module('ngNotebook.article', [
    'ngNotebook.article.edit',
    'ngNotebook.article.create',
    'ngNotebook.services'
]);