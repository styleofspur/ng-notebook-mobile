'use strict';

ngNotebookEditArticle.controller('NgNotebookEditArticleCtrl', function($scope, Article, $sce, $ionicScrollDelegate) {

  $scope.cancelEdit = function() {
    $scope.flags.isUnderEdit = false;
    $ionicScrollDelegate.scrollTop();
  };

  /**
   *
   * @param e
   */
  $scope.onSubmit = function(e) {

    e.preventDefault();

    if ($scope.EditArticleForm.$valid) {

      Article.editArticle($scope.article._id, $scope.editableArticle).then(function(result) {

        if (result) {
          angular.extend($scope.article, $scope.editableArticle);
          $scope.article.content   = $sce.trustAsHtml($scope.article.content);
          $scope.flags.isUnderEdit = false;
          $ionicScrollDelegate.scrollTop();
        }

      });

    }

  }

});