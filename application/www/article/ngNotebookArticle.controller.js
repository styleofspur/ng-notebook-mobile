/**
 * @ngdoc controller
 * @name ArticlesCtrl
 *
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $scope
 * */
ngNotebookArticle.controller('NgNotebookArticleCtrl', ['$scope', 'Article', '$sce', '$state', '$ionicPopup',
  function($scope, Article, $sce, $state, $ionicPopup) {

    $scope.flags = {
      isReady:     false,
      isUnderEdit: false
    };

    /**
     *
     */
    Article.getById($state.params.id).then(function(data) {

      $scope.article         = data;
      $scope.article.content = $sce.trustAsHtml($scope.article.content);
      $scope.editableArticle = {
        title:   data.title,
        content: data.content
      };
      $scope.flags.isReady   = true;

    });

    // On delete btn click
    $scope.deleteArticle = function() {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete Article',
        template: 'Are you sure you want to delete this article?'
      });
      confirmPopup.then(function(res) {

        if (res) {

          Article.deleteArticle($scope.article._id).then(function() { $state.go('root'); });

        }

      });
    };

}]);