# Intro #
[Ionic](http://ionicframework.com/) application for notes management. 
Uses [TinyMCE](https://www.tinymce.com/) text editor for notes creation and update.

# Start #
Simply install Ionic framework globally and run app from the root dir. See [here](http://ionicframework.com/getting-started/) for the instructions.